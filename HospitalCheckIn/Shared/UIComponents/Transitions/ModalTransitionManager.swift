import Foundation
import UIKit

public class ModalTransitionManager: NSObject {
    
    internal var isPresenting: Bool = true
}

extension ModalTransitionManager: UIViewControllerTransitioningDelegate {
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        isPresenting = true
        return self
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        isPresenting = false
        return self
    }
}

extension ModalTransitionManager: UIViewControllerAnimatedTransitioning {
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        
		return isPresenting ? 0.65 : 0.9
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let context = transitionContext
        let containerView = context.containerView
		
		print(containerView.subviews)
        
        guard
            let fromViewController = context.viewController(forKey: .from),
            let toViewController = context.viewController(forKey: .to),
            let fromView = fromViewController.view,
            let toView = toViewController.view
        else { return }
        
        let scaleTransform: CGAffineTransform = .init(scaleX: 0.9, y: 0.9)
        let translationTransform: CGAffineTransform = .init(translationX: 0, y: containerView.bounds.height * 1.1)
        
        let presentedFrame = context.finalFrame(for: toViewController)
        print(presentedFrame)
        
        if isPresenting {
            
            toView.transform = translationTransform
            containerView.addSubview(toView)
            
        } else {
            
            fromView.transform = scaleTransform
        }
        
        let animationDuration = transitionDuration(using: context)
        
		UIView.animate(withDuration: isPresenting ? 0.25 : 0.35, delay: 0, options: .curveEaseOut, animations: {
            
            if self.isPresenting {
                
                fromView.transform = scaleTransform
                
            } else {
				
				fromView.transform = translationTransform
                toView.transform = .identity
            }
            
		}, completion: { finished in
			
			if !self.isPresenting {
				transitionContext.completeTransition(finished)
			}
		})
		
		if !isPresenting { // only
			return
		}
			
        UIView.animate(withDuration: animationDuration, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.6, options: [], animations: {
			
			toView.transform = .identity
            
        }, completion: { finished in
            
            transitionContext.completeTransition(finished)
        })
    }
}
