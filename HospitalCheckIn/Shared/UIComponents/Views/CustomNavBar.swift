//
//  CustomNavBar.swift
//  EatMate
//
//  Created by Arthur Schiller on 04.11.16.
//  Copyright © 2016 Arthur Schiller. All rights reserved.
//

import UIKit
import Foundation

class CustomNavBar: UINavigationBar {
	
	let shadowLayer = CAGradientLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setupStyle()
    }
    
	override func layoutSubviews() {
		super.layoutSubviews()
		shadowLayer.frame = CGRect(x: 0, y: bounds.maxY, width: bounds.width, height: 12)
	}
    
    private func setupStyle() {
		
		shadowLayer.frame = CGRect(x: 0, y: bounds.maxY, width: bounds.width, height: 12)
		shadowLayer.colors = [Theme.Colors.darkGray.withAlphaComponent(0.03).cgColor, Theme.Colors.darkGray.withAlphaComponent(0).cgColor]
		shadowLayer.startPoint = CGPoint(x: 0, y: 0)
		shadowLayer.endPoint = CGPoint(x: 0, y: 1)
		layer.addSublayer(shadowLayer)
        
//        setBackgroundImage(UIImage(), for: .default)
//        isTranslucent = true
		
        // shadow image
		shadowImage = UIImage()
        //shadowImage = UIImage.imageWithColor(color: ThemeColor.darkText.withAlphaComponent(0.2))
        
        // set text and tint colors
        tintColor = Theme.Colors.darkGray
        titleTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: Theme.Colors.darkGray, NSAttributedStringKey.font.rawValue: Theme.Fonts.navBarFont]
        //titleTextAttributes = [NSForeGr: Theme.Colors.darkGray, NSFontAttributeName: Theme.Fonts.navBarFont]
    }
}

class CustomTabBar: UITabBar {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    
    private func commonInit() {
    }
}
