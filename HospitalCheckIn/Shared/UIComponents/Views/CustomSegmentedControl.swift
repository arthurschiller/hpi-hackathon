//
//  CustomSegmentedControl.swift
//  TheFuck
//
//  Created by Arthur Schiller on 16.03.17.
//  Copyright © 2017 Stefan Wirth. All rights reserved.
//

import UIKit

class CustomSegmentedControl: UIControl {
    
    var options: [String] = [] {
        didSet {
            self.cleanUpView()
            self.addLabels()
            self.updateFrames()
        }
    }
    
    var chosenOption: String? {
        if self.options.count <= self.getSelectedIndex() {
            return nil
        }
        return options[self.getSelectedIndex()]
    }
    
    private let minimumValue: Double = 0
    
    private var maximumValue: Double {
        let value = (self.options.count < 2) ? 1 : Double(self.options.count - 1)
        return value
    }
    
    private var value: Double = 0
    
    private lazy var thumbLayer: CustomSegmentedControlThumbLayer = {
        let layer = CustomSegmentedControlThumbLayer()
        layer.segmentedControl = self
        return layer
    }()
    
    private var backgroundLayer = CALayer()
    private var maskLayer = CALayer()
    
    private var scrubbableFrame: CGRect {
        return self.bounds.insetBy(dx: self.thumbWidth / 2, dy: 0)
    }
    
    var thumbColor = Theme.Colors.primary.cgColor {
        didSet {
            self.thumbLayer.backgroundColor = thumbColor
        }
    }
    
    private var didCompleteInitialSetup: Bool = false
    
    private var thumbHeight: CGFloat {
        return self.bounds.height
    }
    
    private var thumbWidth: CGFloat {
        return self.bounds.width / CGFloat(self.options.count > 1 ? self.options.count : 1)
    }
    
    private func normalizeValue(value: Double) -> Double {
        return min(max(value, self.minimumValue), self.maximumValue)
    }
    
    private var thumbPosMaximum: CGPoint {
        return CGPoint(x: self.bounds.width - self.thumbWidth, y: self.thumbLayer.bounds.midY)
    }
    
    private var previousLocation = CGPoint()
    
    private lazy var labelsStackView: UIStackView = {
        return self.makeLabelsStackView()
    }()
    
    private lazy var maskedLabelsStackView: UIStackView = {
        return self.makeLabelsStackView()
    }()
    
    override var frame: CGRect {
        didSet {
            self.updateFrames()
        }
    }
    
    convenience init(options: [String]) {
        self.init()
        self.options = options
        self.commonInit()
        self.addLabels()
        self.updateFrames()
    }
    
    func getSelectedIndex() -> Int {
        return Int(value)
    }
    
    func setSelectedIndex(index: Int) {
        var selectedIndex = Double(index)
        selectedIndex = normalizeValue(value: selectedIndex)
        self.value = selectedIndex
    }
    
    private func makeLabelsStackView() -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }
    
    private func addLabelsToStackView(stackView: UIStackView, textColor: UIColor) {
        for optionTitle in options {
            let label = UILabel()
            label.text = optionTitle
            label.textAlignment = .center
            label.textColor = textColor
            label.font = UIFont(name: "AvenirNext-DemiBold", size: 14) ?? UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)
            stackView.addArrangedSubview(label)
        }
    }
    
    private func addLabels() {
        self.addLabelsToStackView(stackView: labelsStackView, textColor: Theme.Colors.darkGray)
        self.addLabelsToStackView(stackView: maskedLabelsStackView, textColor: UIColor.white)
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.beginTracking(touch, with: event)
        
        self.previousLocation = touch.location(in: self)
        
        if self.thumbLayer.frame.contains(previousLocation) {
            self.thumbLayer.highlighted = true
        }
        
        return self.thumbLayer.highlighted
    }
    
    override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.continueTracking(touch, with: event)
        
        let location = touch.location(in: self)
        
        // Track how much user has dragged
        let deltaLocation = Double(location.x - self.previousLocation.x)
        let width = Double(max(bounds.width - self.thumbLayer.frame.width, 1))
        let deltaValue = (self.maximumValue - self.minimumValue) * deltaLocation / width
        self.previousLocation = location
        
        // update value
        if self.thumbLayer.highlighted {
            self.value += deltaValue
        }
        // update the UI
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        self.updateFrames()
        CATransaction.commit()
        
        return thumbLayer.highlighted
    }
    
    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        super.endTracking(touch, with: event)
        
        // update value
        if self.thumbLayer.highlighted {
            self.value = round(value)
            self.value = normalizeValue(value: value)
        }
        
        // update the UI
        //animateThumbBackToBounds()
        self.updateFrames()
        
        self.sendActions(for: .valueChanged)
        self.thumbLayer.highlighted = false
    }
    
    private func updateFrames() {
        var thumbPos = CGPoint(x: CGFloat(self.value) * ((bounds.width - self.thumbWidth) / CGFloat(self.maximumValue)), y: bounds.midY)
        
        if thumbPos.x + self.thumbWidth < self.thumbWidth {
            let limit: CGFloat = self.thumbWidth
            let logValue = self.logPositionValue(forPosition: fabs(thumbPos.x) + self.thumbWidth, limit: limit)
            thumbPos.x = -logValue + self.thumbWidth
        }
        
        if thumbPos.x > self.thumbPosMaximum.x {
            let limit = self.thumbPosMaximum.x
            let logValue = self.logPositionValue(forPosition: thumbPos.x, limit: limit)
            thumbPos.x = logValue
        }
        
        let thumbFrame = CGRect(x: thumbPos.x, y: bounds.midY - thumbHeight / 2, width: thumbWidth, height: thumbHeight)
        
        self.thumbLayer.frame = thumbFrame
        self.maskLayer.frame = thumbFrame
        
        self.thumbLayer.setNeedsDisplay()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    private func cleanUpView() {
        self.removeArrangedSubviewsFromStackView(stackView: self.labelsStackView)
        self.removeArrangedSubviewsFromStackView(stackView: self.maskedLabelsStackView)
    }
    
    private func removeArrangedSubviewsFromStackView(stackView: UIStackView) {
        stackView.arrangedSubviews.forEach {
            stackView.removeArrangedSubview($0)
            $0.removeFromSuperview()
        }
        print(stackView.arrangedSubviews.count)
    }
    
    private func commonInit() {
        if self.didCompleteInitialSetup {
            return
        }
        
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewWasTapped)))
        
        self.backgroundLayer.cornerRadius = bounds.midY
        self.backgroundLayer.borderWidth = 1
        self.backgroundLayer.borderColor = UIColor.clear.cgColor
        self.backgroundLayer.backgroundColor = UIColor.white.cgColor
        self.layer.addSublayer(backgroundLayer)
        
        backgroundLayer.shadowColor = Theme.Colors.darkGray.cgColor
        backgroundLayer.shadowOpacity = 0.1
        backgroundLayer.shadowOffset = CGSize(width: 0, height: 12)
        backgroundLayer.shadowRadius = 12
        
        self.addViewAndPinToEdges(toView: self, view: self.labelsStackView)
        
        self.thumbLayer.backgroundColor = thumbColor
        self.thumbLayer.cornerRadius = bounds.height / 2
        self.layer.addSublayer(self.thumbLayer)
        
        let maskContainer = UIView()
        self.addViewAndPinToEdges(toView: self, view: maskContainer)
        
        self.maskLayer.backgroundColor = UIColor.black.cgColor
        self.layer.addSublayer(maskLayer)
        
        self.addViewAndPinToEdges(toView: maskContainer, view: self.maskedLabelsStackView)
        maskContainer.layer.mask = maskLayer
        
        self.didCompleteInitialSetup = true
    }
    
    @objc private func viewWasTapped(sender: UITapGestureRecognizer) {
        let tapLocation = Double(sender.location(in: self).x)
        let width = Double(max(bounds.width - thumbLayer.frame.width, 1))
        let calculatedValue = floor((Double(maximumValue) - minimumValue) * tapLocation / width)
        
        self.thumbLayer.highlighted = false
        self.value = calculatedValue
        self.updateFrames()
    }
    
    private func addViewAndPinToEdges(toView: UIView, view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isUserInteractionEnabled = false
        toView.addSubview(view)
        toView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view": view]))
        toView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view": view]))
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.backgroundLayer.frame = bounds
        backgroundLayer.shadowPath = UIBezierPath(roundedRect: backgroundLayer.bounds, cornerRadius: backgroundLayer.cornerRadius).cgPath
        self.updateFrames()
    }
    
    class CustomSegmentedControlThumbLayer: CALayer {
        var highlighted = false
        weak var segmentedControl: CustomSegmentedControl?
    }
    
    private func animateThumbBackToBounds() {
        
        let positionX = self.thumbLayer.position.x
        var targetPositionX: CGFloat = 0
        
        if positionX < 0 {
            targetPositionX = 0
        } else if positionX > bounds.width - self.thumbWidth {
            targetPositionX = self.thumbPosMaximum.x
        }
        
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.65, initialSpringVelocity: 0.6, options: [], animations: {
            self.thumbLayer.position.x = targetPositionX
        }, completion: nil)
    }
    
    private func logPositionValue(forPosition position: CGFloat, limit: CGFloat) -> CGFloat {
        return limit * (1 + log10(position/limit))
    }
}
