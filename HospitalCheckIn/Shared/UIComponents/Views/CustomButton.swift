//
//  CustomButton.swift
//  HospitalCheckIn
//
//  Created by Arthur Schiller on 18.06.17.
//  Copyright © 2017 Arthur Schiller. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func commonInit() {
        clipsToBounds = true
        backgroundColor = Theme.Colors.primary
        titleLabel?.font = Theme.Fonts.buttonFont
        setTitleColor(.white, for: .normal)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = bounds.midY
    }
    
    override func setTitle(_ title: String?, for state: UIControlState) {
        super.setTitle(title?.uppercased(), for: state)
    }
}
