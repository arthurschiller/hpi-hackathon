//
//  CustomTextField.swift
//  HospitalCheckIn
//
//  Created by Arthur Schiller on 18.06.17.
//  Copyright © 2017 Arthur Schiller. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {

    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func commonInit() {
        layer.cornerRadius = Theme.Dimensions.cornerRadius
        layer.borderColor = UIColor.clear.cgColor
        backgroundColor = .white
        
        layer.shadowColor = Theme.Colors.darkGray.cgColor
        layer.shadowOpacity = 0.1
        layer.shadowOffset = CGSize(width: 0, height: 12)
        layer.shadowRadius = 12
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 10)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return self.textRect(forBounds: bounds)
    }
}


//override func textRectForBounds(bounds: CGRect) -> CGRect {
//
//    return CGRectMake(10, bounds.origin.y, bounds.size.width-10, bounds.size.height); // here 10 is padding or spacing from left
//}
//
//override func editingRectForBounds(bounds: CGRect) -> CGRect {
//    return self.textRectForBounds(bounds);
//}

