//
//  HospitalService.swift
//  HospitalCheckIn
//
//  Created by Arthur Schiller on 18.06.17.
//  Copyright © 2017 Arthur Schiller. All rights reserved.
//

import Foundation
import Alamofire

enum Severity: Int {
    case Slight = 1
    case Medium = 2
    case Critical = 3
    case UltraCritical = 4
    case MegaCritical = 5
}

struct Case {
    let userId: Int
    let severity: Severity
    
    init(userId: Int, severity: Severity) {
        self.userId = userId
        self.severity = severity
    }
    
    init(json: [String:AnyObject]) {
        let userId = json["userId"] as! Int
        let severity = Severity(rawValue: json["severity"] as! Int)!
        self.init(userId: userId, severity: severity)
    }
    
    func toJSON() -> [String:Any] {
        return [
            "userId": userId,
            "severity": severity.rawValue
        ]
    }
}

struct Hospital {
    let id: Int
    let name: String
    let lat: Double
    let long: Double
    let distance: Double
    let picture: String
    let predictedWaitTime: Double
    let queue: [Case]
    
    init(json: [String: Any]) {
        self.id = json["id"] as! Int
        self.name = json["name"] as! String
        self.lat = json["lat"] as! Double
        self.long = json["long"] as! Double
        self.distance = json["distance"] as! Double
        self.picture = json["picture"] as! String
        self.predictedWaitTime = json["predictedWaitTime"] as! Double
        self.queue = (json["queue"] as! [[String: AnyObject]]).map({Case(json: $0)})
    }
}

struct HospitalService {
    let baseUrl: String
    
    init(baseUrl: String) {
        self.baseUrl = baseUrl
    }
    
    
    func getAll(cb: @escaping (([Hospital]) -> Void)) {
        Alamofire.request(
            "\(self.baseUrl)/hospitals",
            method: .get,
            parameters: ["lat" : 1, "long" : 1]
            )
            .responseJSON { (res) in
                if let error = res.result.error {
                    print(error)
                    return cb([])
                }
                guard let hospitals = res.result.value as? [[String:AnyObject]] else {
                    return cb([])
                }
                return cb(hospitals.map({Hospital(json: $0)}))
        }
    }
    
    func preCheckin(hospitalId: Int, cb: @escaping ((Double) -> Void)) {
        Alamofire.request(
            "\(self.baseUrl)/preCheckins",
            method: .post,
            parameters: ["hospitalId" : hospitalId],
            encoding: JSONEncoding.default
            )
            .responseJSON { (res) in
                if let error = res.result.error {
                    print(error)
                    return cb(-1)
                }
                guard let _predictedWaitTime = res.result.value as? [String:AnyObject],
                    let predictedWaitTime = _predictedWaitTime["predictedWaitTime"] as? Double  else {
                    return cb(-1)
                }
                cb(predictedWaitTime)
        }
    }
    
    func checkin(hospitalId: Int, ownCase: Case, cb: @escaping ((Hospital?) -> Void)) {
        Alamofire.request(
            "\(self.baseUrl)/checkins",
            method: .post,
            parameters: ["hospitalId" : hospitalId, "user" : ownCase.toJSON()],
            encoding: JSONEncoding.default
            )
            .responseJSON { (res) in
                if let error = res.result.error {
                    print(error)
                    return cb(nil)
                }
                guard let hosptial = res.result.value as? [String:AnyObject] else {
                    return cb(nil)
                }
                cb(Hospital(json: hosptial))
        }
    }
}
