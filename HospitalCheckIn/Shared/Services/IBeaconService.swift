import Foundation

import CoreLocation

protocol IBeaconServiceable {
    var onIBeaconsInRange: ((_ iBeacons: [IBeacon]) -> Void)? { get set }
    var onError: ((_ error: Error) -> Void)? { get set }
    
    func requestAuthorization()
    func startRanging()
}

class IBeaconService: NSObject, IBeaconServiceable {
    
    var onIBeaconsInRange: ((_ iBeacons: [IBeacon]) -> Void)?
    var onError: ((_ error: Error) -> Void)?
    
    private let locationManager: CLLocationManager
    private let monitoredUUID: UUID
    private let beaconIdentifier: String
    
    init(monitoredUUID: UUID, beaconIdentifier: String) {
        self.monitoredUUID = monitoredUUID
        self.beaconIdentifier = beaconIdentifier
        self.locationManager = CLLocationManager()
        super.init()
        self.locationManager.delegate = self
    }
    
    func requestAuthorization() {
        self.locationManager.requestAlwaysAuthorization()
    }
    
    func startRanging() {
        let beaconRegion = CLBeaconRegion(
            proximityUUID: self.monitoredUUID as UUID,
            identifier: self.beaconIdentifier
        )
        self.locationManager.startMonitoring(for: beaconRegion)
        self.locationManager.startRangingBeacons(in: beaconRegion)
    }
}

extension IBeaconService: CLLocationManagerDelegate {
    func locationManager(
        _ manager: CLLocationManager,
        didRangeBeacons beacons: [CLBeacon],
        in region: CLBeaconRegion
        ) {
        let beacons = beacons.map({IBeacon(clBeacon: $0)})
        self.onIBeaconsInRange?(beacons)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Manager failed with error \(error)")
        self.onError?(error)
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Monitoring failed with error \(error)")
        self.onError?(error)
    }
    
    func locationManager(_ manager: CLLocationManager, rangingBeaconsDidFailFor region: CLBeaconRegion, withError error: Error) {
        print("Ranging failed with error \(error)")
        self.onError?(error)
    }
}

struct IBeacon {
    let uuid: String
    let major: Int
    let minor: Int
    let distance: Double
    
    //beacon configured power in dBm
    private let signaldBm: Int = -61
    
    init(clBeacon: CLBeacon) {
        self.uuid = clBeacon.proximityUUID.uuidString
        self.major = clBeacon.major.intValue
        self.minor = clBeacon.minor.intValue
        
        let dbmRatio = self.signaldBm - clBeacon.rssi
        let linearRatio = pow(Double(10), Double(dbmRatio / 10))
        let distance = sqrt(linearRatio)
        self.distance = distance
    }
}
