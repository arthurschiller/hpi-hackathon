//
//  Constants.swift
//  HospitalCheckIn
//
//  Created by Arthur Schiller on 17.06.17.
//  Copyright © 2017 Arthur Schiller. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    static let appName = "QueueConvenient"
}

struct ViewController {
    static let startNavigation = UIStoryboard.init(name: "Main", bundle: nil)
        .instantiateViewController(withIdentifier: Identifier.ViewController.startNavigation)
    
    static let checkInNavigation = UIStoryboard.init(name: "Main", bundle: nil)
        .instantiateViewController(withIdentifier: Identifier.ViewController.checkInNavigation)
    static let checkInStep1 = UIStoryboard.init(name: "Main", bundle: nil)
        .instantiateViewController(withIdentifier: Identifier.ViewController.checkInStep1)
    static let checkInStep2 = UIStoryboard.init(name: "Main", bundle: nil)
        .instantiateViewController(withIdentifier: Identifier.ViewController.checkInStep2)
    static let checkInStep3 = UIStoryboard.init(name: "Main", bundle: nil)
        .instantiateViewController(withIdentifier: Identifier.ViewController.checkInStep3)
    static let checkInStep4 = UIStoryboard.init(name: "Main", bundle: nil)
        .instantiateViewController(withIdentifier: Identifier.ViewController.checkInStep4)
}

struct Identifier {
    struct ViewController {
        static let startNavigation = "startNavigationController"
        static let start = "startViewController"
        
        static let profileNavigation = "profileNavigationController"
        static let profile = "profileViewController"
        
        static let checkInNavigation = "checkInNavigationController"
        static let checkInPage = "checkInPageViewController"
        static let checkInStep1 = "checkInStep1ViewController"
        static let checkInStep2 = "checkInStep2ViewController"
        static let checkInStep3 = "checkInStep3ViewController"
        static let checkInStep4 = "checkInStep4ViewController"
    }
    
    struct CollectionView {
        struct AvailableHospitals {
            static let defaultCell = "availableHospitalsCollectionViewDefaultCell"
        }
    }
    
    struct TableView {
        struct PendingCases {
            static let defaultCell = "pendingCasesDefaultTableViewCell"
        }
    }
}
