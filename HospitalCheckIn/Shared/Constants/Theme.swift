//
//  Theme.swift
//  HospitalCheckIn
//
//  Created by Arthur Schiller on 17.06.17.
//  Copyright © 2017 Arthur Schiller. All rights reserved.
//

import Foundation
import UIKit

struct Theme {
    
    struct Colors {
        static let primary: UIColor = UIColor(hex: "#FE0F6A")!
        static let secondary: UIColor = UIColor(hex: "41D4CB")!
        
        static let background: UIColor = UIColor(hex: "#F5FBF9")!
        
        static let darkGray: UIColor = UIColor(hex: "#373D3D")!
        static let mediumGray: UIColor = UIColor(hex: "#B1BABA")!
        
        static let immediately: UIColor = UIColor(hex: "#FE0F38")!
        static let veryUrgent: UIColor = UIColor(hex: "#FF8500")!
        static let urgent: UIColor = UIColor(hex: "#FFEA12")!
        static let normal: UIColor = UIColor(hex: "#7EFF12")!
        static let notUrgent: UIColor = UIColor(hex: "#12FF9C")!
    }
    
    struct Fonts {
        static let navBarItemFont = UIFont.init(name: "AvenirNext-DemiBold", size: 14) ?? UIFont.boldSystemFont(ofSize: 14)
        static let navBarFont = UIFont.init(name: "AvenirNext-Bold", size: 16) ?? UIFont.boldSystemFont(ofSize: 16)
        static let tabBarFont = UIFont.init(name: "AvenirNext-Regular", size: 11) ?? UIFont.boldSystemFont(ofSize: 11)
        static let buttonFont = UIFont.init(name: "AvenirNext-DemiBold", size: 18) ?? UIFont.boldSystemFont(ofSize: 18)
    }
    
    struct Dimensions {
        static let cornerRadius: CGFloat = 4
    }
}
