//
//  CustomTabBar.swift
//  EatMate
//
//  Created by Arthur Schiller on 05.11.16.
//  Copyright © 2016 Arthur Schiller. All rights reserved.
//

import UIKit

protocol CustomTabBarDataSource {
    func tabBarItemsInCustomTabBar(tabBarView: CustomTabBar) -> [UITabBarItem]
}

protocol CustomTabBarDelegate {
    func didSelectViewController(tabBarView: CustomTabBar, atIndex index: Int)
}

class CustomTabBar: UIView {
    
    // MARK: public properties
	var margins: UIEdgeInsets? {
		didSet {
			
			if let margins = margins {
				tabBarItemsContainerStackView.layoutMargins = margins
			}
		}
	}
	var itemSpacing: CGFloat = 10 {
		didSet {
			tabBarItemsContainerStackView.spacing = itemSpacing
		}
	}
    var blurEffectStyle: UIBlurEffectStyle = .extraLight
    var shadowColor: UIColor = ThemeColor.darkText {
        didSet {
            shadowLayer.colors = [shadowColor.withAlphaComponent(0.05).cgColor, shadowColor.withAlphaComponent(0).cgColor]
        }
    }
	
	var centerViewWidth: CGFloat? {
		didSet {
			
			if let view = centerView {
				
				view.removeConstraints(view.constraints)
				tabBarItemsContainerStackView.distribution = .fillEqually
				
				if let width = centerViewWidth {
					
					let widthConstraint = NSLayoutConstraint(item: view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: width)
					view.addConstraint(widthConstraint)
					widthConstraint.isActive = true
					
					tabBarItemsContainerStackView.distribution = .equalSpacing
				}
			}
		}
	}
    
    var centerView: UIView? {
        didSet {
            let itemsCount = tabBarItems.count
            assert(itemsCount % 2 == 0, "The tabbar items count must be an even number.")
            tabBarItemsContainerStackView.insertArrangedSubview(centerView!, at: itemsCount / 2)
        }
    }
    
    var datasource: CustomTabBarDataSource! {
        didSet {
            setupItems()
        }
    }
    
    var delegate: CustomTabBarDelegate!
    
    var tabBarItems: [UITabBarItem] = []
    var customTabBarItems: [CustomTabBarItem]!
    
    // MARK: private properties
    private var tabBarItemsContainerStackView: UIStackView!
    private var shadowLayer = CAGradientLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        prepareView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        prepareView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        shadowLayer.frame = CGRect(x: 0, y: -12, width: bounds.width, height: 12)
    }
    
    private func prepareView() {
        
        let blurView = UIVisualEffectView(effect: UIBlurEffect(style: blurEffectStyle))
        blurView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(blurView)
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: .init(rawValue: 0), metrics: nil, views: ["view": blurView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options: .init(rawValue: 0), metrics: nil, views: ["view": blurView]))
        
        tabBarItemsContainerStackView = UIStackView(frame: bounds)
        tabBarItemsContainerStackView.axis = .horizontal
        tabBarItemsContainerStackView.spacing = itemSpacing
        tabBarItemsContainerStackView.alignment = .fill
        tabBarItemsContainerStackView.distribution = .fillEqually
        
        tabBarItemsContainerStackView.isLayoutMarginsRelativeArrangement = true
		
		if let margins = margins {
			tabBarItemsContainerStackView.layoutMargins = margins
		} else {
			tabBarItemsContainerStackView.layoutMargins = UIEdgeInsets(top: 0, left: itemSpacing / 2, bottom: 0, right: itemSpacing / 2)
		}
        
        tabBarItemsContainerStackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(tabBarItemsContainerStackView)
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: .init(rawValue: 0), metrics: nil, views: ["view": tabBarItemsContainerStackView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options: .init(rawValue: 0), metrics: nil, views: ["view": tabBarItemsContainerStackView]))
        
		shadowLayer.frame = CGRect(x: 0, y: -12, width: bounds.width, height: 12)
		shadowLayer.colors = [shadowColor.withAlphaComponent(0.05).cgColor, shadowColor.withAlphaComponent(0).cgColor]
		shadowLayer.startPoint = CGPoint(x: 0, y: 1)
		shadowLayer.endPoint = CGPoint(x: 0, y: 0)
        layer.insertSublayer(shadowLayer, above: blurView.layer)
    }
    
    private func setupItems() {
        
        customTabBarItems = []
        
        for subview in tabBarItemsContainerStackView.arrangedSubviews {
            tabBarItemsContainerStackView.removeArrangedSubview(subview)
        }
        
        tabBarItems = datasource.tabBarItemsInCustomTabBar(tabBarView: self)
        
        for (index, item) in tabBarItems.enumerated() {
            
            let customTabBarItem = CustomTabBarItem(item: item, index: index)
            customTabBarItem.tabBar = self
            
            tabBarItemsContainerStackView.addArrangedSubview(customTabBarItem)
			
			customTabBarItem.translatesAutoresizingMaskIntoConstraints = false
			let widthConstraint = NSLayoutConstraint(item: customTabBarItem, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
			customTabBarItem.addConstraint(widthConstraint)

            customTabBarItems.append(customTabBarItem)
        }
    }
}

class CustomTabBarItem: UIView, Bouncable {
    
    weak var tabBar: CustomTabBar?
    
    var index: Int?
    var isSelected: Bool = false {
        
        didSet {
            let color = isSelected ? selectionColor : unselectedColor
			let currentImage = isSelected ? selectedImage : image
			iconView.image = currentImage?.withRenderingMode(.alwaysTemplate)
            iconView.tintColor = color
            titleLabel.textColor = color
        }
    }
    
    var unselectedColor: UIColor = ThemeColor.darkText
    var selectionColor: UIColor = ThemeColor.primary
    
    let iconView = UIImageView()
    let titleLabel = UILabel()
	
	var image: UIImage?
	var selectedImage: UIImage?
    
    weak var centerView: UIView?
	
	private let bounceAnimationKey = "bounceAnimation"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureView()
    }
    
    convenience init(item: UITabBarItem, index: Int) {
        self.init()
        
        self.index = index
        setupWithItem(item: item)
    }
    
    func configureView() {
		
        tintColor = ThemeColor.darkText
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(itemTapped(sender:)))
		tapGestureRecognizer.delaysTouchesBegan = false
        addGestureRecognizer(tapGestureRecognizer)
    }
    
    private func setupWithItem(item: UITabBarItem) {
        
        // add icon image view
        iconView.contentMode = .center
        iconView.translatesAutoresizingMaskIntoConstraints = false
        iconView.tintColor = unselectedColor
        addSubview(iconView)
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[view(==25)]", options: .init(rawValue: 0), metrics: nil, views: ["view": iconView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view(==25)]", options: .init(rawValue: 0), metrics: nil, views: ["view": iconView]))
        
        addConstraint(NSLayoutConstraint(item: iconView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: iconView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 6))
        
        if let image = item.image {
			self.image = image.withRenderingMode(.alwaysTemplate)
            iconView.image = self.image
        }
		
		if let selectedImage = item.selectedImage {
			self.selectedImage = selectedImage.withRenderingMode(.alwaysTemplate)
		}
		
        // add title label
        titleLabel.font = UIFont.systemFont(ofSize: 9, weight: UIFontWeightMedium)
        titleLabel.textAlignment = .center
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(titleLabel)
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: .init(rawValue: 0), metrics: nil, views: ["view": titleLabel]))
        addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: iconView, attribute: .bottom, multiplier: 1, constant: 3.5))
        
        if let title = item.title {
            titleLabel.text = title
            titleLabel.textColor = unselectedColor
        }
    }
    
	func itemTapped(sender: UITapGestureRecognizer) {

		bounce(toRelativeSize: CGPoint(x: 0.8, y: 0.8), animationKey: bounceAnimationKey, completion: nil)
		
		delayWithSeconds(0.12, completion: {
			self.bounce(toRelativeSize: CGPoint(x: 1, y: 1), animationKey: self.bounceAnimationKey, completion: nil)
		})
		
        guard
			let index = self.index,
			let tabBarView = tabBar
		else {
            print("This item does not have an index or associated tabbarview set.")
            return
        }
		
        print("Item at index: \(index) was selected")
		tabBar?.delegate.didSelectViewController(tabBarView: tabBarView, atIndex: index)
    }
}
