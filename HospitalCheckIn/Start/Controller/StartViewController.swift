//
//  StartViewController.swift
//  HospitalCheckIn
//
//  Created by Arthur Schiller on 17.06.17.
//  Copyright © 2017 Arthur Schiller. All rights reserved.
//

import UIKit
import MBCircularProgressBar

class StartViewController: UIViewController {
    
    @IBOutlet weak var checkInButton: UIButton!
    @IBOutlet weak var checkInSuccesfulScreen: CheckInOverviewScreen!
    
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    
    @IBOutlet weak var timeIndicator: MBCircularProgressBarView!
    @IBOutlet weak var tableView: UITableView!
    
    private let transitionManager = ModalTransitionManager()
    
    private var hospital: Hospital? = nil
    
    private lazy var iBeaconService: IBeaconServiceable = IBeaconService(
        monitoredUUID: UUID(uuidString: "F0018B9B-7509-4C31-A905-1A27D39C003C")!,
        beaconIdentifier: "beaconInside"
    )
    
    let hospitalService = HospitalService(
        baseUrl: "https://1qyjx3es76.execute-api.us-east-1.amazonaws.com/dev"
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        toggleInfoOverlay(show: false, animated: false)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.contentInset.bottom = 49
        tableView.scrollIndicatorInsets.bottom = 49
        tableView.separatorColor = UIColor.white
        
        title = Constants.appName
        
        checkInSuccesfulScreen.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(checkInSuccesfulScreen)
        
        checkInSuccesfulScreen.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        checkInSuccesfulScreen.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        checkInSuccesfulScreen.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        checkInSuccesfulScreen.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
//        checkInSuccesfulScreen.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.timeIndicator.value = 0
        
        UIView.animate(withDuration: 1.2, delay: 0, options: .curveEaseOut, animations: {
            self.timeIndicator.value = 115
        }, completion: nil)
    }
    
    private func toggleInfoOverlay(show: Bool, animated: Bool) {
        
        UIView.animate(withDuration: animated ? 0.25 : 0, delay: 0, options: .curveEaseOut, animations: {
//            self.visualEffectView.effect = show ? UIBlurEffect(style: .extraLight) : nil
            self.checkInSuccesfulScreen.alpha = show ? 1 : 0
            self.checkInSuccesfulScreen.transform = CGAffineTransform.init(scaleX: show ? 1 : 0.8, y: show ? 1 : 0.8)
        }, completion: nil)
    }
}

extension StartViewController {
    
    @IBAction func checkInButtonTapped(sender: Any) {
        
        let checkInNavController = ViewController.checkInNavigation
        checkInNavController.transitioningDelegate = transitionManager
        checkInNavController.modalPresentationStyle = .custom
        
        let pageViewController = checkInNavController.childViewControllers[0] as! CheckInPageViewController
        pageViewController.checkInDelegate = self
        
        print(checkInNavController.childViewControllers)
        
        present(checkInNavController, animated: true, completion: nil)
    }
}

extension StartViewController: CheckInPageViewControllerDelegate {
    func checkInWasConfirmedForHospital(hospital: Hospital) {
        
        self.hospital = hospital
        self.tableView.reloadData()
        self.timeIndicator.maxValue = CGFloat(Int(hospital.predictedWaitTime))
        self.timeIndicator.value = CGFloat(Int(hospital.predictedWaitTime))
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.toggleInfoOverlay(show: true, animated: true)
            self.iBeaconService.onIBeaconsInRange = { beacons in
                guard let beacon = beacons.first else {
                    return
                }
                
                if beacon.distance > 1 {
                    return
                }
                self.showAlertForHospital(hospital: hospital)
            }
            
            self.iBeaconService.requestAuthorization()
            self.iBeaconService.startRanging()
        })
    }
    
    private func showAlertForHospital(hospital: Hospital) {
        let controller = UIAlertController(
            title: "Are you at \(hospital.name)",
            message: "Press OK to check in at \(hospital.name)",
            preferredStyle: .alert
        )
        let action = UIAlertAction(title: "OK", style: .default) { [unowned self] action in
            self.hospitalService.checkin(
                hospitalId: hospital.id,
                ownCase: Case(userId: 100, severity: .Medium)
            ) { (hospital) in
                print(hospital)
            }
        }
        controller.addAction(action)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
}

extension StartViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hospital?.queue.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.TableView.PendingCases.defaultCell, for: indexPath) as? PendingCasesTableViewCell else {
            return UITableViewCell()
        }
        let queueCase = self.hospital!.queue[indexPath.row].severity.rawValue
        let urgency = PendingCasesTableViewCell.Urgency(rawValue: queueCase)!
        cell.viewData = PendingCasesTableViewCell.ViewData(urgency: urgency)
        return cell
    }
}

class CheckInOverviewScreen: UIView {
    
    @IBOutlet weak var delayButton15Min: UIButton!
    @IBOutlet weak var delayButton30Min: UIButton!
    @IBOutlet weak var delayButton45Min: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    
    private func commonInit() {
        
    }
}

class WaitTimeIndicator: UIView {
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    
    private func commonInit() {
        
    }
}
