//
//  CustomNavBar.swift
//  EatMate
//
//  Created by Arthur Schiller on 04.11.16.
//  Copyright © 2016 Arthur Schiller. All rights reserved.
//

import UIKit

class CustomNavBar: UINavigationBar {
	
	let shadowLayer = CAGradientLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupStyle()
    }
    
    override func prepareForInterfaceBuilder() {
        
        setupStyle()
    }
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		shadowLayer.frame = CGRect(x: 0, y: bounds.maxY, width: bounds.width, height: 12)
	}
    
    private func setupStyle() {
		
		shadowLayer.frame = CGRect(x: 0, y: bounds.maxY, width: bounds.width, height: 12)
		shadowLayer.colors = [ThemeColor.darkText.withAlphaComponent(0.05).cgColor, ThemeColor.darkText.withAlphaComponent(0).cgColor]
		shadowLayer.startPoint = CGPoint(x: 0, y: 0)
		shadowLayer.endPoint = CGPoint(x: 0, y: 1)
		layer.addSublayer(shadowLayer)
        
        setBackgroundImage(UIImage(), for: .default)
        isTranslucent = true
		
        // shadow image
		shadowImage = UIImage()
        //shadowImage = UIImage.imageWithColor(color: ThemeColor.darkText.withAlphaComponent(0.2))
        
        // set text and tint colors
        tintColor = ThemeColor.primary
        titleTextAttributes = [NSForegroundColorAttributeName: ThemeColor.darkText, NSFontAttributeName: UIFont.systemFont(ofSize: 17, weight: AppStyles.barTextFontWeight)]
    }
}
