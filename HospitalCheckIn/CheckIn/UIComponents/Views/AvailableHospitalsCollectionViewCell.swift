//
//  AvailableHospitalsCollectionViewCell.swift
//  HospitalCheckIn
//
//  Created by Arthur Schiller on 18.06.17.
//  Copyright © 2017 Arthur Schiller. All rights reserved.
//

import UIKit

class AvailableHospitalsCollectionViewCell: UICollectionViewCell {
    
    struct ViewData {
        let image: UIImage
        let title: String
        let waitingTime: Double
        let distance: Double
    }
    
    var viewData: ViewData? {
        didSet {
            guard let viewData = self.viewData else {
                return
            }
            //imageView.image = viewData.image
            titleLabel.text = viewData.title
            waitingTimeLabel.text = String(Int(viewData.waitingTime)) + " minutes"
            distanceLabel.text = String(viewData.distance) + " km"
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var waitingTimeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var cardShadowView: UIView!
    @IBOutlet weak var cardContainerView: UIView!
    
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        commonInit()
    }
    
    private func commonInit() {
        
        cardContainerView.layer.cornerRadius = Theme.Dimensions.cornerRadius
        cardContainerView.clipsToBounds = true
        
        cardShadowView.layer.shadowColor = Theme.Colors.darkGray.cgColor
        cardShadowView.layer.shadowOpacity = 0.1
        cardShadowView.layer.shadowOffset = CGSize(width: 0, height: 8)
        cardShadowView.layer.shadowRadius = 6
        
        imageView.layer.cornerRadius = Theme.Dimensions.cornerRadius
        imageView.clipsToBounds = true
        
        imageContainer.layer.shadowColor = Theme.Colors.darkGray.cgColor
        imageContainer.layer.shadowOpacity = 0.1
        imageContainer.layer.shadowOffset = CGSize(width: 0, height: 12)
        imageContainer.layer.shadowRadius = 10
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        cardShadowView.layer.shadowPath = UIBezierPath(roundedRect: cardShadowView.bounds, cornerRadius: cardShadowView.layer.cornerRadius).cgPath
        imageContainer.layer.shadowPath = UIBezierPath(roundedRect: imageContainer.bounds, cornerRadius: imageContainer.layer.cornerRadius).cgPath
    }
}
