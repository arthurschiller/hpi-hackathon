//
//  PendingCasesTableViewCell.swift
//  HospitalCheckIn
//
//  Created by Arthur Schiller on 18.06.17.
//  Copyright © 2017 Arthur Schiller. All rights reserved.
//

import UIKit

class PendingCasesTableViewCell: UITableViewCell {
    
    enum Urgency: Int {
        case immediately = 5
        case veryUrgent = 4
        case urgent = 3
        case normal = 2
        case notUrgent = 1
    }
    
    struct ViewData {
        let urgency: Urgency
    }
    
    var viewData: ViewData? {
        didSet {
            guard let viewData = self.viewData else {
                return
            }
            
            switch viewData.urgency {
            case .immediately:
                self.titleLabel.text = "Immediately"
                self.contentView.backgroundColor = Theme.Colors.immediately
            case .veryUrgent:
                self.titleLabel.text = "Very Urgent"
                self.contentView.backgroundColor = Theme.Colors.veryUrgent
            case .urgent:
                self.titleLabel.text = "Urgent"
                self.contentView.backgroundColor = Theme.Colors.urgent
            case .normal:
                self.titleLabel.text = "Normal"
                self.contentView.backgroundColor = Theme.Colors.normal
            case .notUrgent:
                self.titleLabel.text = "Not urgent"
                self.contentView.backgroundColor = Theme.Colors.notUrgent
            }
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        commonInit()
    }
    
    private func commonInit() {
        let view = UIView()
        view.backgroundColor = self.contentView.backgroundColor?.withAlphaComponent(0.2)
        
        selectedBackgroundView = view
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
