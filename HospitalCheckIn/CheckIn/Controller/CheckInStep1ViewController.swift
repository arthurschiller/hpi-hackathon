//
//  CheckInStep1ViewController.swift
//  HospitalCheckIn
//
//  Created by Arthur Schiller on 17.06.17.
//  Copyright © 2017 Arthur Schiller. All rights reserved.
//

import UIKit

class CheckInStep1ViewController: CheckInFlowViewController {
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var painLevelControl: CustomSegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        commonInit()
    }
    
    private func commonInit() {
        painLevelControl.options = ["Slight", "Medium", "Intense"]
        painLevelControl.backgroundColor = .clear
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        textField.endEditing(true)
    }
}
