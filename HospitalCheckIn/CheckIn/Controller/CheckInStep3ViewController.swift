//
//  CheckInStep1ViewController.swift
//  HospitalCheckIn
//
//  Created by Arthur Schiller on 17.06.17.
//  Copyright © 2017 Arthur Schiller. All rights reserved.
//

import UIKit
import Alamofire
import MapKit

protocol CheckInStep3ViewControllerDelegate: class {
    func didSelectHospital(hospital: Hospital)
}

class CheckInStep3ViewController: CheckInFlowViewController {
    
    weak var delegate: CheckInStep3ViewControllerDelegate? = nil
    
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var resultsCollectionView: UICollectionView!
    
    let initialLocation = CLLocation(latitude: 52.393787, longitude: 13.131836)
    let regionRadius: CLLocationDistance = 10000
    let hospitalService = HospitalService(
        baseUrl: "https://1qyjx3es76.execute-api.us-east-1.amazonaws.com/dev"
    )
    
    private var hospitals: [Hospital] = []
    
    private lazy var fadeMaskLayer: CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor.black.cgColor, UIColor.black.withAlphaComponent(0.15).cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 0, y: 1)
        gradient.locations = [0, 0.75]
        return gradient
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        commonInit()
        
        self.hospitalService.getAll { (hospitals) in
            self.hospitals = hospitals
            self.resultsCollectionView.reloadData()
            self.reloadMap()
        }
    }
    
    func reloadMap() {
        self.hospitals.forEach { (hospital) in
            let coordinate = CLLocationCoordinate2D(latitude: hospital.lat, longitude: hospital.long)
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            mapView.addAnnotation(annotation)
        }
        //reload coordinates on the map and set pins
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        fadeMaskLayer.frame = mapContainerView.bounds
    }
    
    private func commonInit() {
        resultsCollectionView.dataSource = self
        resultsCollectionView.delegate = self
        
        centerMapOnLocation(location: initialLocation)
        
        mapContainerView.layer.mask = fadeMaskLayer
    }
    
    private func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
}

extension CheckInStep3ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.hospitals.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifier.CollectionView.AvailableHospitals.defaultCell, for: indexPath) as? AvailableHospitalsCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        let hospital = self.hospitals[indexPath.item]
        
        Alamofire.request(hospital.picture)
            .responseData { res in
                guard let data = res.data else {
                    return
                }
                cell.imageView.image = UIImage(data: data)
        }
        
        cell.viewData = AvailableHospitalsCollectionViewCell.ViewData(
            image: UIImage(),
            title: hospital.name,
            waitingTime: hospital.predictedWaitTime,
            distance: hospital.distance
        )
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width - 30, height: 220)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let hospital = self.hospitals[indexPath.item]
        self.delegate?.didSelectHospital(hospital: hospital)
    }
}
