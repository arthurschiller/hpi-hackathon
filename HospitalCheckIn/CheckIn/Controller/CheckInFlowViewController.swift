//
//  CheckInFlowViewController.swift
//  HospitalCheckIn
//
//  Created by Arthur Schiller on 17.06.17.
//  Copyright © 2017 Arthur Schiller. All rights reserved.
//

import UIKit
import MapKit

class CheckInFlowViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        commonInit()
    }
    
    private func commonInit() {
        view.backgroundColor = Theme.Colors.background
    }
}
