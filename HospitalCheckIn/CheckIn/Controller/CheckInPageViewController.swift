//
//  CheckInPageViewController.swift
//  HospitalCheckIn
//
//  Created by Arthur Schiller on 17.06.17.
//  Copyright © 2017 Arthur Schiller. All rights reserved.
//

import UIKit

protocol CheckInPageViewControllerDelegate: class {
    func checkInWasConfirmedForHospital(hospital: Hospital)
}

class CheckInPageViewController: UIPageViewController {
    
    weak var checkInDelegate: CheckInPageViewControllerDelegate?
    
    private var scrollEnabled: Bool = false
    private var currentIndex: Int = 0
    private var hospital: Hospital? = nil
    
    private lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "prev-btn"), for: .normal)
        button.setTitleColor(Theme.Colors.darkGray, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(prevButtonPressed), for: .touchUpInside)
        button.widthAnchor.constraint(equalToConstant: 64).isActive = true
        button.heightAnchor.constraint(equalToConstant: 19).isActive = true
        return button
    }()
    
    private lazy var nextButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "next-btn"), for: .normal)
        button.setTitleColor(Theme.Colors.darkGray, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
        button.widthAnchor.constraint(equalToConstant: 38).isActive = true
        button.heightAnchor.constraint(equalToConstant: 38).isActive = true
        return button
    }()
    
    let step3ViewController = ViewController.checkInStep3
    let step4ViewController = ViewController.checkInStep4
    
    private lazy var orderedViewControllers: [UIViewController] = {
        return [
            ViewController.checkInStep1,
//            ViewController.checkInStep2,
            self.step3ViewController,
            self.step4ViewController
        ]
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        commonInit()
    }
    
    private func toggleView(view: UIView, show: Bool) {
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: {
            view.alpha = show ? 1 : 0
        }, completion: nil)
    }
    
    private func commonInit() {
        view.backgroundColor = .white
        backButton.alpha = 0
        
        let closeItem = UIBarButtonItem(title: "Cancel".uppercased(), style: .plain, target: self, action: #selector(cancelButtonTapped))
        closeItem.setTitleTextAttributes([NSAttributedStringKey.foregroundColor.rawValue: Theme.Colors.darkGray, NSAttributedStringKey.font.rawValue: Theme.Fonts.navBarItemFont], for: .normal)
        
        
        
        navigationItem.leftBarButtonItem = closeItem
        title = "Check-In"
        
        (step4ViewController as? CheckInStep4ViewController)?.delegate = self
        (step3ViewController as? CheckInStep3ViewController)?.delegate = self
        dataSource = self
        
        switchToPage(index: 0, isForwards: true)
        
//        let hospitalsViewController = orderedViewControllers[2]
//        setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        
        view.addSubview(backButton)
        view.addSubview(nextButton)
        
        let buttonMargin: CGFloat = 15
        
        backButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: buttonMargin).isActive = true
        backButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        
        nextButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -buttonMargin).isActive = true
        nextButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -buttonMargin).isActive = true
    }
}

extension CheckInPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
    private func switchToPage(index: Int, isForwards: Bool) {
        
        if index == 0 {
            toggleView(view: backButton, show: false)
        } else {
            toggleView(view: backButton, show: true)
        }
        
        if index == orderedViewControllers.count - 1 {
            toggleView(view: nextButton, show: false)
        } else {
            toggleView(view: nextButton, show: true)
        }
        
        guard index < orderedViewControllers.count else {
            print("unavailable index")
            return
        }
        currentIndex = index
        print(currentIndex)
        setViewControllers([orderedViewControllers[index]], direction: isForwards ? .forward : .reverse
            , animated: true, completion: nil)
    }
}

extension CheckInPageViewController {
    
   @objc private func cancelButtonTapped() {
        dismiss(animated: true, completion: {
            self.switchToPage(index: 0, isForwards: true)
        })
    }
    
    @objc private func prevButtonPressed() {
        if currentIndex == 0 {
            return
        }
        
        switchToPage(index: currentIndex - 1, isForwards: false)
    }
    
    @objc private func nextButtonPressed() {
        switchToPage(index: currentIndex + 1, isForwards: true)
    }
}

extension CheckInPageViewController: UIPageViewControllerDelegate {
    
}

extension CheckInPageViewController: CheckInStep4ViewControllerDelegate {
    func confirmButtonWasTapped() {
        checkInDelegate?.checkInWasConfirmedForHospital(hospital: self.hospital!)
        dismiss(animated: true, completion: {
            self.switchToPage(index: 0, isForwards: true)
        })
    }
}

extension CheckInPageViewController: CheckInStep3ViewControllerDelegate {
    func didSelectHospital(hospital: Hospital) {
        print(hospital)
        self.hospital = hospital
        self.nextButtonPressed()
    }
}
