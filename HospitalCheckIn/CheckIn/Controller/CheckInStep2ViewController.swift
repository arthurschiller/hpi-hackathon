//
//  CheckInStep1ViewController.swift
//  HospitalCheckIn
//
//  Created by Arthur Schiller on 17.06.17.
//  Copyright © 2017 Arthur Schiller. All rights reserved.
//

import UIKit

class CheckInStep2ViewController: CheckInFlowViewController {
    
    @IBOutlet weak var bodyImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        commmonInit()
    }
    
    private func commmonInit() {
        bodyImageView.backgroundColor = .blue
    }
}
