//
//  CheckInStep1ViewController.swift
//  HospitalCheckIn
//
//  Created by Arthur Schiller on 17.06.17.
//  Copyright © 2017 Arthur Schiller. All rights reserved.
//

import UIKit

protocol CheckInStep4ViewControllerDelegate: class {
    func confirmButtonWasTapped()
}

class CheckInStep4ViewController: CheckInFlowViewController {
    
    @IBOutlet weak var confirmButton: UIButton!
    weak var delegate: CheckInStep4ViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        commonInit()
    }
    
    private func commonInit() {
        
    }
}

extension CheckInStep4ViewController {
    
    @IBAction func confirmButtonTapped(sender: Any) {
        print("dismiss")
        delegate?.confirmButtonWasTapped()
    }
}
